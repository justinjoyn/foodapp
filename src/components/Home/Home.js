import React, {Component} from 'react';
import {Button, Grid, Input, Message} from 'semantic-ui-react'
import {foodAddAction, foodDeleteAction, foodListAction} from '../../actions'
import {connect} from 'react-redux';


class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount() {
        this.props.getFoodItem();
    }

    handleDismiss = (id) => {
        // this.setState({ visible: false })
        this.props.removeFoodItem(id);
    }

    handleChange = (event) => {
        this.setState({
            food_name: event.target.value,
            error: /[^a-zA-Z\s]/.test(event.target.value)
        })
    }

    addFoodItem = () => {
        this.props.addFood({name: this.state.food_name});
    }

    handleContent() {
        if (this.props.foodItemState.loading) {
            return (<div>Loading.....</div>)
        }
        else if (this.props.foodItemState.data.Items) {
            return (
                <div className="row food-list">
                    {this.props.foodItemState.data.Items.map((item, key) => <Message
                        key={key}
                        onDismiss={this.handleDismiss.bind(this, item.foodId)}
                        header={item.name || "-"}
                        content={item.dateCreated || "-"}
                        icon='food'
                    />)}
                </div>
            )
        }
        return []
    }

    render() {

        return (
            <div className="App">
                <Grid centered columns={2}>
                    <Grid.Column>
                        <Input type='text' placeholder='Add Food...' className="food-input" onChange={this.handleChange}
                               action>
                            <input/>
                            <Button disabled={this.state.error} type='button' onClick={this.addFoodItem}>Add</Button>
                        </Input>
                        {this.state.error && <Message color='red'>Only letters and space are allowed</Message>}
                        {this.handleContent()}
                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}


const mapStateToProps = (state, ownProps) => {
    console.log(state)
    return {
        foodItemState: state.main.foodReducer
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        addFood: (props) => {
            dispatch(foodAddAction(props))
        },
        getFoodItem: (props) => {
            dispatch(foodListAction(props))
        },
        removeFoodItem: (id) => {
            dispatch(foodDeleteAction(id))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);